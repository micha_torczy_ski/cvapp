package com.example.cvapplication.cv

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.cvapplication.R
import java.util.ArrayList

class CvAdapter(items: ArrayList<CVitem>) : RecyclerView.Adapter<CvAdapter.ViewHolder>() {

    var mdata: List<CVitem>? = null

    fun CVAdapter(mdata: List<CVitem>?) {
        this.mdata = mdata
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_cv, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tvTitle.setText(mdata!![position].getTitle())
        holder.tvDescription.setText(mdata!![position].getDescription())
    }

    override fun getItemCount(): Int {
        return mdata!!.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvTitle: TextView
        var tvDescription: TextView

        init {
            tvTitle = itemView.findViewById(R.id.item_cv_title)
            tvDescription = itemView.findViewById(R.id.item_cv_desc)
        }
    }
}