package com.example.cvapplication.cv

class CVitem(string: String, string1: String) {
    private var title: String? = null
    private var description: String? = null

    fun CVItem(title: String?, description: String?) {
        this.title = title
        this.description = description
    }

    fun CVItem() {}

    fun getTitle(): String? {
        return title
    }

    fun setTitle(title: String?) {
        this.title = title
    }

    fun getDescription(): String? {
        return description
    }

    fun setDescription(description: String?) {
        this.description = description
    }
}
