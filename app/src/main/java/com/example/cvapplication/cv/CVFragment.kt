package com.example.cvapplication.cv

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.cvapplication.R
import java.util.*

class CVFragment: Fragment() {
    lateinit var RvCv: RecyclerView
    var adapter: CvAdapter? = null
    var items: MutableList<CVitem>? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        RvCv = view.findViewById(R.id.recyclerview_cv)

        // create a list of cv items
        items = ArrayList<CVitem>()
        items!!.add(CVitem(getString(R.string.xelo_date_text), getString(R.string.xelo_discription_text)))
        items!!.add(CVitem(getString(R.string.samsung_date_text), getString(R.string.samsung_discription_text)))
        items!!.add(CVitem(getString(R.string.softax_date_text), getString(R.string.softax_discription_text)))
        items!!.add(CVitem(getString(R.string.omtech_date_text), getString(R.string.omtech_discription_text)))

        adapter = CvAdapter(items as ArrayList<CVitem>)
        RvCv.setLayoutManager(LinearLayoutManager(context))
        RvCv.setAdapter(adapter)
    }

    fun CVFragment() {
        // Required empty public constructor
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.cv_fragment, container, false)
    }

}
