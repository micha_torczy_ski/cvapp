package com.example.cvapplication.menu

import com.example.cvapplication.R
import java.util.*
import kotlin.collections.ArrayList

object Utils {

    val HOME_FRAGMENT_CODE = 0
    val CV_FRAGMENT_CODE = 1


    fun getMenuList(): List<Utils> {
        val list: ArrayList<MenuItem> = ArrayList()
        // first menu item is selected
        list.add(MenuItem(R.drawable.ic_baseline_home_24, HOME_FRAGMENT_CODE, true))
        list.add(MenuItem(R.drawable.ic_baseline_school_24, CV_FRAGMENT_CODE, false))
        return list
    }


}