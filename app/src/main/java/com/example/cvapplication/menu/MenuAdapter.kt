package com.example.cvapplication.menu

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.example.cvapplication.MainActivity
import com.example.cvapplication.R

class MenuAdapter(Utils: Utils, mainActivity: MainActivity) : RecyclerView.Adapter<MenuAdapter.ViewHolder>() {
    var mData: List<MenuItem>? = null
    lateinit var listener: MenuCallback


    fun MenuAdapter(mData: List<MenuItem>?, listener: MenuCallback) {
        this.mData = mData
        this.listener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.item_menu, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.icon.setImageResource(mData!![position].getIcon())
        if (mData!![position].isSelected()) {
            holder.isSelected.visibility = View.VISIBLE
        } else holder.isSelected.visibility = View.GONE
    }

    override fun getItemCount(): Int {
        return mData!!.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var icon: ImageView
        var isSelected: ImageView

        init {
            icon = itemView.findViewById(R.id.item_menu_icon)
            isSelected = itemView.findViewById(R.id.item_menu_selected)

        }
    }
}
