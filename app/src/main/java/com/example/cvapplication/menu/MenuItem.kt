package com.example.cvapplication.menu

class MenuItem(icBaselineHome24: Int, HOME_FRAGMENT_CODE: Int, b: Boolean) {
    private var icon = 0
    private  var code:Int = 0
    private var isSelected = false

    fun MenuItem(icon: Int, code: Int) {
        this.icon = icon
        this.code = code
    }

    fun MenuItem(icon: Int, code: Int, isSelected: Boolean) {
        this.icon = icon
        this.code = code
        this.isSelected = isSelected
    }

    fun getIcon(): Int {
        return icon
    }

    fun setIcon(icon: Int) {
        this.icon = icon
    }

    fun getCode(): Int {
        return code
    }

    fun setCode(code: Int) {
        this.code = code
    }

    fun isSelected(): Boolean {
        return isSelected
    }

    fun setSelected(selected: Boolean) {
        isSelected = selected
    }
}