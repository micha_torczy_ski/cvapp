package com.example.cvapplication

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.cvapplication.cv.CVFragment
import com.example.cvapplication.home.HomeFragment
import com.example.cvapplication.menu.MenuAdapter
import com.example.cvapplication.menu.MenuCallback
import com.example.cvapplication.menu.Utils
import com.example.cvapplication.menu.Utils.getMenuList

class MainActivity : AppCompatActivity(), MenuCallback {

    lateinit var menuRv: RecyclerView
    lateinit var menuItems: List<Utils>
    lateinit var adapter: MenuAdapter
    var selectedMenuPos = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar!!.hide()

        // setup side menu
        setupSideMenu()

        // set the default fragment when activity launch
        setHomeFragment()
    }

    private fun setupSideMenu() {
        menuRv = findViewById(R.id.menu)

        // get menu list item  will get data from a static data class
        menuItems = getMenuList()
        adapter = MenuAdapter(Utils, this)
        menuRv.setLayoutManager(LinearLayoutManager(this))
        menuRv.setAdapter(adapter)
    }


    fun setCVFragment() {
        supportFragmentManager.beginTransaction().replace(R.id.container, CVFragment()).commit()
    }


    fun setHomeFragment() {
        supportFragmentManager.beginTransaction().replace(R.id.container, HomeFragment()).commit()
    }


    override fun onSideMenuItemClick(i: Int) {
/*        when (menuItems!![i].getCode()) {
            Utils.HOME_FRAGMENT_CODE -> setHomeFragment()
            Utils.CV_FRAGMENT_CODE -> setCVFragment()
            else -> setHomeFragment()
        }

        // hightligh the selected menu item
        menuItems!![selectedMenuPos].setSelected(false)
        menuItems!![i].setSelected(true)
        selectedMenuPos = i
        adapter?.notifyDataSetChanged()*/
    }
}